rarbg
Backup of magnets from RARBG

Currently:
clean.py is my Python script for cleaning up magnets post-extraction. I think it might have some finnicky thing going on with the way it fixes two magnets in one line, but it works.
moviesrarbg.txt holds my original post, cleaned up a lot. (117,392)
showsother.txt holds my original post, cleaned up a little. (137,671)
showsrarbg.txt holds my original post, cleaned up a lot. (11,699)
everything.7z holds what i've compiled so far from some of the sources given me (3,459,526)

I'm confident that some of the stuff in everything.7z did not come from RARBG, and that will be my first step to remedy once I get everything in there.
I'm about a fourth of the way done adding to everything.7z. Then I'll filter it and sort it and split it into its relevant categories. Alongside the movies and shows, it also holds porn, music, and games, which will each get new .txt files.
Thanks guys.

This repository was mentioned on TorrentFreak - https://torrentfreak.com/rarbg-over-267000-movie-tv-show-magnet-links-appear-online-230601/
